## Overview

This [Cypress](https://www.cypress.io/) plugin can help you in the following cases.

**1** Your test configuration depends on multiple factors.

For example, `apiBaseUrl` differs per environment (dev, staging etc).

**2** You need to be able to combine multiple configuration files together, allowing for overrides among configurations.

**3** You need to store sensitive information (e.g., login credentials) in configuration files that should not be versioned (e.g., ignored by Git).

## Setup

**1** Install through Yarn:

```bash
yarn add --dev cypress-configuration-builder
```

or NPM:

```bash
npm install --dev cypress-configuration-builder
```

**2** Open your Cypress [plugins file](https://docs.cypress.io/guides/core-concepts/writing-and-organizing-tests.html#Plugin-files) (usually, `/cypress/plugins/index.js`) and add:

```js
const ccb = require('cypress-configuration-builder');

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => ccb.buildConfiguration(config);
```

## Example

```powershell
yarn start --env configKeys=a.b
```

This will load the following files:

1. `cypress.json` (loaded by default - see [Cypress Configuration](https://docs.cypress.io/guides/references/configuration.html#Options))
2. `cypress.a.json`
3. `cypress.a-secrets.json`
4. `cypress.b.json`
5. `cypress.b-secrets.json`

## Explanation

**1** Except for `cypress.json`, the rest of the file names follow these patterns:

1. either: `cypress.<configKey>.json`
2. or: `cypress.<configKey>-secrets.json`

**2** The configuration files we want to combine are defined in the `configKeys` [environment variable](https://docs.cypress.io/guides/guides/environment-variables.html#Option-4-env).

**2.1** The file keys are separated by a dot ([U+002E](https://www.compart.com/en/unicode/U+002E)) character.

**2.2** The separator can change through the [configKeysSeparator](#configKeysSeparator) parameter.

**2.3** 2.1 and 2.2 imply that we cannot have the `configKeysSeparator` string in a file name's `configKey` part.

For example, if `configKeysSeparator` is the dot character, we cannot have a configuration file named `cypress.a.2.json` because passing it to `configKeys`:

```bash
yarn run cypress run --env configKeys=a.2
```

would result in loading `cypress.a.json` and `cypress.2.json`, instead of `cypress.a.2.json`.

**3** The order the files are defined in the call (`--configKeys`) matters.

Later files override earlier ones. For example, assuming we have the following two files:

```js
// File: cypress.json

{
    "baseUrl": "http://default.com"
}
```

```js
// File: cypress.a.json

{
    "baseUrl": "http://a.com"
}
```

`baseUrl` will end up equal to: http://a.com.

**3.1** The files are combined with [deep merging](https://lodash.com/docs/4.17.15#merge).

For example, assuming the following files:

```js
// File: cypress.json

{
    "a": {
        "b": {
            "c": 1
        },
        "d": 2
    }
}
```

```js
// File: cypress.a.json

{
    "a": {
        "b": {
            "c": 9
        }
    }
}
```

we will end up with this configuration:

```json
{
    "a": {
        "b": {
            "c": 9
        },
        "d": 2
    }
}
```

**4** Except for `cypress.json`, if any of the other files does not exist, it is simply ignored.

## Best practices

**1** Use the `cypress.json` file for parameters that should be shared by all tests.

**2** Use environment (e.g., `cypress.dev.json`) specific files for parameters specific to an environment.

**3** Use the `-secrets` files for configuration you want available only on your local machine. For example, login credentials.

> **Note** Remember to ignore the `-secrets` files from Git in order to avoid leaking sensitive information.
>
> For example, add this entry to your `.gitignore` file:
>
> ```
> cypress.*-secrets.json
> ```

**4** You are not restricted to per environment configuration files.

Feel free to mix and match in order to create a configuration that suits your needs.

For example, let's say we have the following file:

```js
// File: cypress.a.json

{
    "a": 2,
    "b": 1
}
```

And we want to run a test that will have:

1. `a` equal to 1 just for this test
2. keep the rest of the configuration in this file (in this case, just `b`)

We can simply create a new configuration file, let's call it `cypress.a-2.json`, where we will define `a` equal to 1:

```js
// File: cypress.a-2.json

{
    "a": 1
}
```

Then, we can use it like this:

```powershell
yarn start --env configKeys=a.a-2
```

This will give us the expected configuration:

```json
{
    "a": 1,
    "b": 1
}
```

**5** Consider using the [Cypress configuration file JSON schema](https://docs.cypress.io/guides/tooling/IDE-integration.html#Set-up-in-your-Dev-Environment-1) in your configuration files for autocompletion.

## Configuration

<a name="configKeys"></a> **configKeys** Indicates the configuration files to be loaded, in addition to the one Cypress loads by default (e.g., `cypress.json`).

For example:

```bash
yarn run cypress run --env configKeys=a.b
```

will merge: `cypress.json`, `cypress.a.json`, `cypress.a-secrets.json`, `cypress.b.json` and `cypress.b-secrets.json`.



<a name="configKeysSeparator"></a> **configKeysSeparator** Specifies the string that will be used as a separator for the values of `configKeys`.

For example:

```bash
yarn run cypress run --env configKeys=a#b,configKeysSeparator=#
```

Notice, that we now use the number sign character (#) as a separator in `configKeys`.

> Invalid separators:
>
> - | (pipe - [U+007C](https://www.fileformat.info/info/unicode/char/7c/index.htm))
> - , (comma - [U+002C](https://www.fileformat.info/info/unicode/char/002c/index.htm))

## Tests

In order to execute the test suite run:

```bash
yarn test
```

