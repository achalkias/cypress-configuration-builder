import { buildConfiguration, CypressPluginConfig } from '../src/index';
import { Configuration } from './fixtures/Configuration';

describe('configuration-builder', () => {
  const defaultCypressConfig: CypressPluginConfig = Configuration.cypressDefaultConfiguration;

  it('should return the Cypress configuration (`cypress.json` by default) as-is if no `configKeys` is specified', async () => {
    const configuration = await buildConfiguration(defaultCypressConfig);

    expect(configuration).toEqual(defaultCypressConfig);
  });

  it.each([
    {
      pluginConfig: {
        configKeys: 'a'
      },
      expectedBaseUrl: 'https://example.com/a',
      expectedEnv: {
        a: 'a'
      },
      toString: () => 'should merge "cypress.a.json" when `configKeys`="a" and "a-secrets" file does not exist'
    },
    {
      pluginConfig: {
        configKeys: 'b'
      },
      expectedBaseUrl: 'https://example.com/b',
      expectedEnv: {
        b: 'b-secret'
      },
      toString: () =>
        'should merge "cypress.b.json" and "cypress.b-secrets.json" when configKeys="b" and "b-secrets" file exists'
    },
    {
      pluginConfig: {
        configKeys: 'b.c'
      },
      expectedBaseUrl: 'https://example.com/c',
      expectedEnv: {
        b: 'c'
      },
      toString: () => 'should override "cypress.b.json" with "cypress.c.json" when configKeys="b.c"'
    },
    {
      pluginConfig: {
        configKeys: 'deep-a.deep-a2'
      },
      expectedBaseUrl: 'https://example.com/deep-a',
      expectedEnv: {
        a: {
          a1: 1,
          a2: {
            a21: 'deep-a2'
          }
        }
      },
      toString: () => 'should perform deep merge'
    },
    {
      pluginConfig: {
        configKeys: 'nonExistent'
      },
      expectedBaseUrl: defaultCypressConfig.baseUrl,
      expectedEnv: { ...defaultCypressConfig.env, configKeys: 'nonExistent' },
      toString: () => 'should ignore any non-existent files'
    },
    {
      pluginConfig: {
        configKeys: 'b|c',
        configKeysSeparator: '|'
      },
      expectedBaseUrl: 'https://example.com/c',
      expectedEnv: {
        b: 'c'
      },
      toString: () => 'should use the provided `configKeysSeparator` when one is specified'
    }
  ])('%s', async ({ pluginConfig, expectedBaseUrl, expectedEnv }) => {
    const cypressConfig = {
      ...defaultCypressConfig,
      env: { ...defaultCypressConfig.env, ...pluginConfig }
    };

    const configuration = await buildConfiguration(cypressConfig);

    expect(configuration.baseUrl).toBe(expectedBaseUrl);
    expect(configuration.env).toMatchObject(expectedEnv);
  });
});
