import { realpathSync } from 'fs-extra';
import { CypressPluginConfig } from '../../src/index';

export namespace Configuration {
  export const cypressDefaultConfiguration: CypressPluginConfig = {
    animationDistanceThreshold: 0,
    baseUrl: 'https://example.com',
    chromeWebSecurity: false,
    configFile: 'cypress.json',
    defaultCommandTimeout: 0,
    env: {
      configKeys: undefined
    },
    execTimeout: 0,
    experimentalGetCookiesSameSite: false,
    experimentalSourceRewriting: false,
    fileServerFolder: '',
    firefoxGcInterval: {
      runMode: 1,
      openMode: null
    },
    fixturesFolder: '',
    ignoreTestFiles: '*.hot-update.js',
    integrationFolder: '',
    nodeVersion: 'bundled',
    numTestsKeptInMemory: 0,
    pageLoadTimeout: 0,
    pluginsFile: '',
    port: null,
    projectRoot: realpathSync('./tests/fixtures'),
    reporter: '',
    requestTimeout: 0,
    resolvedNodePath: '',
    resolvedNodeVersion: '',
    responseTimeout: 0,
    screenshotsFolder: '',
    supportFile: '',
    trashAssetsBeforeRuns: false,
    video: false,
    videoCompression: 0,
    videoUploadOnPasses: false,
    videosFolder: '',
    viewportHeight: 0,
    viewportWidth: 0,
    waitForAnimations: false,
    watchForFileChanges: false
  };
}
