import { pathExists, readJSON } from 'fs-extra';
import { merge } from 'lodash';
import { resolve } from 'path';
import PluginConfigOptions = Cypress.PluginConfigOptions;

/**
 * The default string used for separating the keys specified in the `configKeys` parameter.
 *
 * This is used when the user does not set the `configKeysSeparator` parameter.
 */
const defaultConfigKeysSeparator = '.';

/**
 * The suffix that identifies the secrets files.
 */
const secretsFileSuffix = '-secrets';

/**
 * Contains all the configuration parameters specific to this plugin.
 */
interface ConfigurationBuilderConfig {
  configKeys: string;
}

/**
 * An alias for the configuration expected as input to this plugin.
 *
 * This is the configuration created by Cypress during runtime and passed to its plugins.
 *
 * @see https://docs.cypress.io/api/plugins/configuration-api.html
 */
export type CypressPluginConfig = PluginConfigOptions & { env: Partial<ConfigurationBuilderConfig> };

/**
 * Merges the configuration files denoted by `cypressConfig.env.configKeys` to `cypressConfig`.
 *
 * ## Example
 * ```bash
 * yarn run cypress run --env configKeys=a.b.c
 * ```
 *
 * This `configKeys` value indicates to this method that it needs to merge the following files:
 * - `cypress.a.json`
 * - `cypress.a-secrets.json`
 * - `cypress.b.json`
 * - `cypress.b-secrets.json`
 * - `cypress.c.json`
 * - `cypress.c-secrets.json`
 *
 * In the above list, later files override earlier ones. So, if the same parameter is found both in
 * `cypress.c-secrets.json` and in `cypress.a.json`, the end file will have the value from `cypress.c-secrets.json`.
 *
 * ## Secrets files
 * Notice how in the `configKeys` parameter we only specified `a`, and not `a-secrets`.
 *
 * The corresponding secrets files are loaded automatically by this method, if they exist.
 *
 * @param cypressConfig The configuration loaded by Cypress from the default configuration file (usually
 * "cypress.json").
 */
export const buildConfiguration = async (cypressConfig: CypressPluginConfig): Promise<PluginConfigOptions> => {
  const { configKeys, configKeysSeparator } = cypressConfig.env;

  const filesPaths = buildFilesPathsFromKeys(cypressConfig.projectRoot, configKeys, configKeysSeparator);

  return filesPaths.reduce<Promise<PluginConfigOptions>>(async (config, filePath) => {
    const fileConfig = (await pathExists(filePath)) ? await parseConfigFile(filePath) : {};

    return merge(await config, fileConfig);
  }, Promise.resolve(cypressConfig));
};

/**
 * For each key (configKey) of `configKeys` this method creates two file paths:
 * 1. one for the `cypress.<configKey>.json` file
 * 2. one for the `cypress.<configKey>-secrets.json` file.
 *
 * @param configKeys A list of configuration keys separated by the `configKeysSeparator`.
 *
 * For example: "a.b.c", given that `configKeysSeparator` is the dot character (.).
 *
 * @param projectRoot This is usually where the `cypress.json` file is (see [Configuration API - Usage](https://docs.cypress.io/api/plugins/configuration-api.html#Usage)).
 *
 * @param [configKeysSeparator=defaultConfigKeysSeparator] The string that is used in order to separate the multiple
 * values passed to `configKeys`.
 */
const buildFilesPathsFromKeys = (projectRoot: string, configKeys?: string, configKeysSeparator?: string): string[] => {
  configKeysSeparator = configKeysSeparator ?? defaultConfigKeysSeparator;

  const configKeysArray = configKeys?.length ? configKeys.split(configKeysSeparator) : [];

  return configKeysArray.reduce((files: string[], configKey) => {
    const filePath = parseKeyToFilePath(configKey, projectRoot);
    const secretsFilePath = parseKeyToFilePath(configKey, projectRoot, { isSecretsFile: true });

    return [...files, filePath, secretsFilePath];
  }, []);
};

const parseConfigFile = (filePath: string): Promise<PluginConfigOptions> => readJSON(filePath);

const parseKeyToFilePath = (
  configKey: string,
  projectRoot: string,
  { isSecretsFile = false }: Partial<{ isSecretsFile: boolean }> = {}
): string => {
  configKey += isSecretsFile ? secretsFileSuffix : '';

  const fileName = ['cypress', configKey, 'json'].filter(Boolean).join('.');

  return resolve(projectRoot, fileName);
};
